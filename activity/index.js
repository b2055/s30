const express = require("express")
const mongoose = require("mongoose")
const app = express()
const port = 3001

mongoose.connect("mongodb+srv://admin:Password30x__@cluster0.s5jd2.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database."))
app.use(express.json())
app.use(express.urlencoded({extended:true}))

const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

const User = mongoose.model("User", userSchema)

/* 
register a user (Business Logic)
1. Check for duplicates
    -if user already exists, we return an error
    -if user doesn't exist, we add it in database
        -if the username and password are both not blank
            -if blank, send response "BOTH username and password must be provided"
            -both condition has been met, create a new object.
            -Save the new object
                -if error, return an error message
                -else, response a status 201 and "New User Registered"
*/

app.post("/signup", (req, res) => {
    User.findOne({username:req.body.username}, (error, result)=> {
        if (result == null || result.username == null || result.password == null || result.username == "" || result.password == "") {
            return res.send("Please provide both username and password.");
        } else if(result !== null && result.username == req.body.username) {
            return res.send("Username is already used."); 
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password,
            })

            newUser.save((saveerror, savedUser)=>{
                if(saveerror){
                    return console.error(saveerror);
                } else {
                    return res.status(201).send("You've successfully created an account")
                }
            });
        }
    })
})

/*
Business Logic GET
    1. Find the data.
    2. Check if there's an error.
    3. If there's no error then retrieve the data.
*/

app.get("/users", (req, res) => {
    User.find( {}, (err,result) => {
        if(err) {
            return console.log(err)
        } else {
            return res.status(200).json({
                data:result
            })
        }
    })
})

app.listen(port, () => console.log(`Server is running at post ${port}`))