const express = require("express")

//
const mongoose = require("mongoose")


const app = express()
const port = 3001

//MongoDB Connection
//Connecting to mongoDB Atlas
mongoose.connect("mongodb+srv://admin:Password30x__@cluster0.s5jd2.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

//Set notifications for connection success or failure
let db = mongoose.connection;
//if error occured, output in the console
db.on("error", console.error.bind(console, "connection error"))
//if the connection is successful, out in the console
db.once("open", () => console.log("We're connected to the cloud database."))

app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Mongoose Schemas

//Schema determine the structure of the documents to be written in the database
//it acts as a blueprints to our data
//We will use Schema() constructor of the Mongoose module to create a new Schema object

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        //Default values are the predefined values for a field if we don't put any value
        default: "pending"
    }
})

const Task = mongoose.model("Task", taskSchema)
//Models are what allows us to gain access to methods taht will perform CRUD functions.
//Models must be singular form and capitalized following the MVC approach for naming conventions
//firstparamter of the mongoose model method indicates the collection in where to store the data
//second parameter is used to specify the Schema/blueprint

//Routes

//Create a new task
/* 
    Business Logic
    
    1. Add a functionality to check if there are duplicates tasks
        -if the task already exists, return there is a duplicate
        -if the task doesn't exist, we can add it in the database
    2. The task data will be coming from the request's body
    3. Create new Task object with properties what we need
    4. Then save the data
*/

app.post("/tasks", (req, res) => {
    //Check if there are duplicates tasks
    //findOne() is a mongoose method that acts similar to "find"
    //it returns the first document that matches the search criteria
    Task.findOne({name:req.body.name}, (error, result)=> {
        //if a document was found and the document's name matches the information
        // sent via client/postman
        if(result !== null && result.name == req.body.name) {
            // return 
    
            return res.send("Duplicate Task Found");
        } else {
            // no found
            // create a new task
            let newTask = new Task({
                name: req.body.name,
                // status: req.body.status,
            })
            newTask.save((saveerror, savedTask)=>{
                // if error in saving
                // saveerror = error object details
    
                if(saveerror){
                    return console.error(saveerror);
                } else {
                    // no error found
                    // 201 - successfully created
                    return res.status(201).send("New Task Created")
                }
            });
        }
    })
})

/* 
Retrieving all data
Business Logic
    1. Retrive all the documents using the find()
    2. If an error is encountered, print the error
    3. If no errors are found, send a success status back to the client and return an array of documents
*/

app.get("/tasks", (req, res) => {
    //an empty "{}" means it returns all the documents and stores them in the "result" parater of the call back function
    Task.find( {}, (err,result) => {
        //if an error occured
        if(err) {
            return console.log(err + "test")
        } else {
            return res.status(200).json({
                data:result
            })
        }
    })
})

const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

const User = mongoose.model("User", userSchema)

/* 
register a user (Business Logic)
1. Check for duplicates
    -if user already exists, we return an error
    -if user doesn't exist, we add it in database
        -if the username and password are both not blank
            -if blank, send response "BOTH username and password must be provided"
            -both condition has been met, create a new object.
            -Save the new object
                -if error, return an error message
                -else, response a status 201 and "New User Registered"
*/

app.post("/signup", (req, res) => {
    User.findOne({username:req.body.username}, (error, result)=> {
        if (result == null || result.username == null || result.password == null || result.username == "" || result.password == "") {
            return res.send("Please provide both username and password.");
        } else if(result !== null && result.username == req.body.username) {
            return res.send("Username is already used."); 
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password,
            })

            newUser.save((saveerror, savedUser)=>{
                if(saveerror){
                    return console.error(saveerror);
                } else {
                    return res.status(201).send("You've successfully created an account")
                }
            });
        }
    })
})

/*
Business Logic GET
    1. Find the data.
    2. Check if there's an error.
    3. If there's no error then retrieve the data.
*/

app.get("/users", (req, res) => {
    User.find( {}, (err,result) => {
        if(err) {
            return console.log(err)
        } else {
            return res.status(200).json({
                data:result
            })
        }
    })
})

app.listen(port, () => console.log(`Server is running at post ${port}`))